var path = require('path');
var GasPlugin = require("gas-webpack-plugin");
var HtmlPlugin = require("html-webpack-plugin");

module.exports = {
  mode: 'development',
  entry: {
	  main: './src/index/action.ts'
	},
  devtool: false,
  output: {
    filename: '[name].js',
    path: path.join(__dirname, 'dist')
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: 'ts-loader'
      }
    ]
  },
  resolve: {
    extensions: [
      '.ts', '.tsx'
    ]
  },
  plugins: [
	 new GasPlugin(),
	 new HtmlPlugin({
     inject: false,
		 filename: 'index.html',
		 template: './src/html/index.html'
	 }),
	 new HtmlPlugin({
     inject: false,
		 filename: 'stylesheet.html',
		 template: './src/html/stylesheet.html'
	 }),
	 new HtmlPlugin({
     inject: false,
		filename: 'javascript.html',
		template: './src/html/javascript.html'
	})
  ]
};
