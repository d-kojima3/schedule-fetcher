export class SheetConfig {
  static sheets: object = {
    '1': {
      name: '企画4企画5制作3スケジュール管理表',
      url:
        'https://docs.google.com/spreadsheets/d/16RY2NH7vYoD2UED7tz-FZPfSrQzlpvBDKjZubD_PerI/edit#'
    },
    '2': {
      name: '第3開発チームスケジュール表',
      url:
        'https://docs.google.com/spreadsheets/d/1kX97eaKozAoRKKkAnieq--1SBLpxHdG478gfGXK4xdM/edit#'
    }
  };

  static periodToFetch: number = 7;
}

export class UnavailableRangeError extends Error {}

export type ResponseType = {
  date: string;
  project_name: string;
  archive_name: string;
  work_name: string;
  task_time: number;
  sheet_name: string;
  sheet_row: string;
};
