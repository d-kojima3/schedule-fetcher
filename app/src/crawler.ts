import Spreadsheet = GoogleAppsScript.Spreadsheet.Spreadsheet;
import Utilities = GoogleAppsScript.Utilities.Utilities;
import Sheet = GoogleAppsScript.Spreadsheet.Sheet;
import { Supplier } from './supplier';
import { SheetConfig, UnavailableRangeError, ResponseType } from './sheet.config';

export class Crawler {
  protected supplier: Supplier;
  protected activePrjt: ResponseType[][];
  protected activeDatePos: number;
  protected activeRowValues: object;
  protected targetUrl: string;
  protected spreadSheet: Sheet;
  protected startRow: number;
  protected endRow: number;
  protected startCol: number;
  protected endCol: number;
  protected myName: string;
  protected set_date: Date;
  protected date_diff: number;

  constructor(supplier: Supplier) {
    this.supplier = supplier;

    // 自分のスプレッドシート上で設定されている日付
    this.set_date = supplier.beginningOfPeriod;

    if (this.set_date > this.supplier.endOfPeriod) {
      throw new UnavailableRangeError('期間の指定に誤りがあります。');
    }
    let cached_date: Date = new Date(this.set_date.toString());
    cached_date.setDate(cached_date.getDate() + SheetConfig.periodToFetch);
    if (cached_date < this.supplier.endOfPeriod) {
      throw new UnavailableRangeError('期間の指定に誤りがあります。');
    }
    // オブジェクトが参照渡しになってしまうのを回避するため
    for (let index = 0; index <= SheetConfig.periodToFetch; index++) {
      let cached_date: Date = new Date(this.set_date.toString());
      cached_date.setDate(cached_date.getDate() + index);
      if (cached_date.getDate() === this.supplier.endOfPeriod.getDate()) {
        this.date_diff = index;
        break;
      }
    }

    this.targetUrl = supplier.sheetUrl();
    this.activePrjt = [];

    do {
      if (supplier.sheetName) {
        this.spreadSheet = SpreadsheetApp.openByUrl(this.targetUrl).getSheetByName(
          supplier.sheetName
        );
        break;
      }
      if (supplier.sheetIndex) {
        this.spreadSheet = SpreadsheetApp.openByUrl(this.targetUrl).getSheets()[
          supplier.sheetIndex
        ];
        break;
      }
      this.spreadSheet = SpreadsheetApp.openByUrl(this.targetUrl).getSheets()[0];
    } while (false);

    // スケジュール表の範囲（何列目から始まるか）
    this.startCol = supplier.startOfColumn;

    // スケジュール表の範囲（最終列）
    this.endCol = this.spreadSheet.getLastColumn();

    // スケジュール表の範囲（何行目から始まるか）
    this.startRow = supplier.startOfRow;

    // スケジュール表の範囲（最終行）
    this.endRow = this.spreadSheet.getLastRow();

    // 検索対象者
    this.myName = supplier.familyName;
  }

  /**
   * alias of method fetching result.
   *
   */
  public getTaskDaily(): ResponseType[][] {
    return this.initDateCol().fetchList().Result;
  }

  protected initDateCol(): this {
    // 日付の形式
    var today: string = Utilities.formatDate(this.set_date, 'Asia/Tokyo', 'yyyy/MM/dd');

    // 日付のレンジ
    var date_range: GoogleAppsScript.Spreadsheet.Range = this.spreadSheet.getRange(
      2,
      this.startCol,
      1,
      this.endCol
    );
    // 日付の値
    var date_vals: Object[][] = date_range.getValues();
    var format_dates = date_vals[0].map(child => {
      if (typeof child !== 'object') return '';
      let _child = child as Date;
      return Utilities.formatDate(_child, 'Asia/Tokyo', 'yyyy/MM/dd');
    });

    for (var i = 0, len = this.endCol; i < len; i = (i + 1) | 0) {
      var d_val = format_dates[i] as string;
      if (today === d_val) {
        // 今日の日付の列
        this.activeDatePos = i + this.startCol;
        this.activeRowValues = this.spreadSheet
          .getRange(this.startRow, this.activeDatePos, this.endRow, this.date_diff + 1)
          .getValues();
        break;
      }
    }
    return this;
  }

  protected fetchList(): this {
    var active_range: GoogleAppsScript.Spreadsheet.Range = this.spreadSheet.getRange(
      this.startRow,
      1,
      this.endRow,
      this.supplier.staffColumnIdx + 1
    );

    // スケジュールの情報があるレンジ
    var list_range: Object[][] = active_range.getValues();

    var sheet_name = this.spreadSheet.getName();

    // 商材リストのスタイル取得
    var list_fontSize_range: number[][] = active_range.getFontSizes();
    var list_fontWeight_range: string[][] = active_range.getFontWeights();

    // ループ内で一時的に保存させるための変数
    var cache_project: string, cache_archive: string, cache_taskname: string;

    for (var col = 0; col <= this.date_diff; col = (col + 1) | 0) {
      var projects_day: ResponseType[] = [];
      var activeDate: Date = new Date(this.set_date.toString());
      activeDate.setDate(activeDate.getDate() + col);

      for (var row = 0, len = list_range.length; row < len; row = (row + 1) | 0) {
        var numberNow: number = this.activeRowValues[row][col];
        var columnIdx: number = this.startRow + row;

        // font-size
        let fzs = list_fontSize_range[row];
        // font-weight
        let fws = list_fontWeight_range[row];
        // active column
        let lrng = list_range[row];

        // indexes
        let category_index = this.supplier.categoryColumnIdx;
        let project_index = this.supplier.projectColumnIdx;
        let task_index = this.supplier.taskColumnIdx;
        let staff_index = this.supplier.staffColumnIdx;

        // タスク名、プロジェクト名などを一時的に保存
        if (fzs[project_index] > 16 && lrng[project_index] !== '') {
          cache_project = lrng[project_index].toString();
        } else if (fws[project_index] === 'bold' && fzs[project_index] > 7) {
          cache_archive = lrng[project_index].toString();
        } else if (!this.isNumber(numberNow) || numberNow === 0) {
          continue;
        }

        // 自分の名前が存在すれば配列へ格納
        if (lrng[staff_index] == this.supplier.familyName) {
          // タスク内容
          cache_taskname = lrng[task_index].toString();
          projects_day.push({
            date: Utilities.formatDate(activeDate, 'Asia/Tokyo', 'yyyy/MM/dd'),
            project_name: this.decode(cache_project),
            archive_name: cache_archive,
            work_name: cache_taskname,
            task_time: numberNow,
            sheet_name: sheet_name,
            sheet_row: columnIdx.toString()
          });
        }
      }
      if (projects_day.length > 0) this.activePrjt.push(projects_day);
    }

    return this;
  }

  get Result(): ResponseType[][] {
    return this.activePrjt;
  }

  private decode(prjName: string): string {
    var project_category: string;
    prjName = prjName
      .trim()
      .replace(/\u3010/, '')
      .replace(/\u3011/, '');
    switch (prjName) {
      case 'WN':
      case 'フレッツ光':
      case 'ドコモ光':
      case 'So-net光':
      case 'So-netマルチ':
      case '全戸プラン（ベストエフォート）':
        project_category = '第一事業本部-第一事業部-フレッツ光';
        break;
      case '比較ネット':
        project_category = '第一事業本部-第一事業部-比較ネット';
        break;
      case 'auひかり25(kddi-hikari.com)':
      case 'auひかり25':
        project_category = '第一事業本部-第二事業部-auひかり（25）';
        break;
      case 'auひかりJC(au-hikarinet.com)':
      case 'auひかりJC':
        project_category = '第一事業本部-第二事業部-auひかり（JC）';
        break;
      case 'auひかりパイオン':
      case 'auひかり パイオン':
        project_category = '第一事業本部-第二事業部-auひかり（パイオン）';
        break;
      case 'ソフトバンク光':
      case 'ソフトバンク光 LB':
        project_category = '第一事業本部-第三事業部-SoftBank光';
        break;
      case ' ソフトバンク光(ST)':
      case 'ソフトバンク光(ST)':
      case 'ソフトバンク光 ST':
        project_category = '第一事業本部-第三事業部-SoftBank光';
        break;
      case 'WY':
      case 'YBB':
      case 'フレッツADSL':
        project_category = '第一事業本部-第三事業部-YBB';
        break;
      case 'WNURO光(コラサポ)':
      case 'NURO光（CS）':
        project_category = '第一事業本部-第二事業部-NURO光(CS)';
        break;
      case 'WNURO光(LifeBank)':
      case 'NURO光（LB）':
        project_category = '第一事業本部-第二事業部-NURO光(LB)';
        break;
      case 'Wメガ':
      case 'メガエッグ':
        project_category = '第一事業本部-第二事業部-メガエッグ';
        break;
      case 'BBIQ2':
        project_category = '不明な案件';
        break;
      case 'Wピカ':
      case 'ピカラ':
        project_category = '第一事業本部-第二事業部-ピカラ';
        break;
      case 'Sコミュ光':
      case 'コミュファ':
        project_category = '第一事業本部-第二事業部-コミュファ光';
        break;
      case 'NETdePC':
      case 'NETdeパソコン':
        project_category = '第二事業本部-第一事業部-NETdePC';
        break;
      case 'スマセレクト':
        project_category = '第二事業本部-第二事業部-スマセレクト';
        break;
      case 'SBM':
      case 'SBM3':
        project_category = '第二事業本部-本部-SBM';
        break;
      case 'モバナビ':
        project_category = '第二事業本部-本部-モバナビ';
        break;
      case 'モバレコ':
        project_category = '不明な案件';
        break;
      case '引越達人':
      case '引越し達人':
      case '引越し':
      case '引っ越し案件':
        project_category = '第一事業本部-引越事業部-引っ越しの達人（セレクト）';
        break;
      case '保険コネクト':
      case '保険コネクト案件':
        project_category = '第一事業本部-保険事業部-保険コネクト';
        break;
      case 'WimaxBroad':
      case 'WiMAXショット':
      case 'MVNOショット':
      case 'ショット差し込み':
        project_category = '第三事業本部-第一事業部-MVNOショット';
        break;
      case 'Wi-Fi STORE':
      case 'WiFiストア':
        project_category = '第三事業本部-第一事業部-Wi-Fi STORE ';
        break;
      case 'WiMAXストック':
      case 'MVNOストック':
      case 'FVNOストック':
        project_category = '第一事業本部-ストック事業部-MVNOカスタマー';
        break;
      case 'PCV':
        project_category = '第一事業本部-ストック事業部-PCV';
        break;
      case '水OEMaquano':
        project_category = '第一事業本部-ストック事業部-水OEM';
        break;
      case '電力OEM':
        project_category = '第一事業本部-ストック事業部-電力OEM';
        break;
      case 'UQ mobile モバレコ店':
        project_category = '第三事業本部-第二事業部-UQ mobile（モバレコ）';
        break;
      case 'UQ mobile':
      case 'UQmobile':
        project_category = '第三事業本部-第二事業部-UQ mobile';
        break;
      case 'BC':
        project_category = 'BC本部-広報-広報';
        break;
      case 'ITS':
        project_category = '不明な案件';
        break;
      case '都道府県リスト修正':
        project_category = '不明な案件';
        break;
      case 'アフィリエイト':
        project_category = '不明な案件';
        break;
      case '固定費':
      case '調整費':
      case '日時業務':
      case '作業確認':
      case 'その他':
      case 'KPI':
      case '個別':
      case 'MTG':
      case '事故・差込対応':
      case '管理':
        project_category = '固定費-管理';
        break;
      default:
        project_category = '不明な案件';
        break;
    }
    return project_category;
  }

  private isNumber(value: any): boolean {
    return value == parseFloat(value) && isFinite(value);
  }
}
