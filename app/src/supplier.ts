import { SheetConfig } from './sheet.config';

export class Supplier {
  protected sheet_id: number;
  protected sheet_index: number;
  protected sheet_name: string;
  protected family_name: string;
  protected start_column: number;
  protected start_row: number;
  protected period_beginning: Date;
  protected period_end: Date;
  protected column_project: number;
  protected column_category: number;
  protected column_task: number;
  protected column_staff: number;

  get sheetId() {
    return this.sheet_id;
  }
  get sheetIndex() {
    return this.sheet_index;
  }
  get sheetName() {
    return this.sheet_name;
  }
  get familyName() {
    return this.family_name;
  }
  get startOfColumn() {
    return this.start_column;
  }
  get startOfRow() {
    return this.start_row;
  }
  get beginningOfPeriod() {
    return this.period_beginning;
  }
  get endOfPeriod() {
    return this.period_end;
  }
  get projectColumnIdx() {
    return this.column_project;
  }
  get categoryColumnIdx() {
    return this.column_category;
  }
  get taskColumnIdx() {
    return this.column_task;
  }
  get staffColumnIdx() {
    return this.column_staff;
  }

  public sheetUrl(): string {
    if (this.sheet_id) return SheetConfig.sheets[this.sheetId].url;
  }

  public setSheetId(id: number): this {
    this.sheet_id = id;
    return this;
  }
  public setSheetIndex(index: number): this {
    this.sheet_index = index;
    return this;
  }
  public setSheetName(name: string): this {
    this.sheet_name = name;
    return this;
  }
  public setFamilyName(name: string): this {
    this.family_name = name;
    return this;
  }
  public setStartOfColumn(column: number): this {
    this.start_column = column;
    return this;
  }
  public setStartOfRow(row: number): this {
    this.start_row = row;
    return this;
  }
  public setBeginningPeriod(setDate: Date): this {
    this.period_beginning = setDate;
    return this;
  }
  public setEndPeriod(setDate: Date): this {
    this.period_end = setDate;
    return this;
  }
  public setProjectColumnIdx(index: number): this {
    this.column_project = index;
    return this;
  }
  public setCategoryColumnIdx(index: number): this {
    this.column_category = index;
    return this;
  }
  public setTaskColumnIdx(index: number): this {
    this.column_task = index;
    return this;
  }
  public setStaffColumnIdx(index: number): this {
    this.column_staff = index;
    return this;
  }
}
