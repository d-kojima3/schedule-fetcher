import { Supplier } from '../supplier';
import { Crawler } from '../crawler';
import './action';

declare var global: any;

global.testSupplierGettingUrl = () => {
  var builder = new Supplier();
  builder
    .setFamilyName('小嶋')
    .setSheetId(1)
    .setStartOfColumn(9)
    .setStartOfRow(11)
    .setBeginningPeriod(new Date('July 4, 2018'));
  var b = builder.sheetUrl();
};

global.testGetData2 = () => {
  var _date = '2018-09-03';
  var builder = new Supplier();
  builder
    .setSheetId(1)
    .setFamilyName('小嶋')
    .setBeginningPeriod(new Date(_date + 'T00:00:00'))
    .setEndPeriod(new Date('2018-09-10T00:00:00'));

  builder
    .setStartOfColumn(9)
    .setStartOfRow(3)
    .setCategoryColumnIdx(0)
    .setProjectColumnIdx(0)
    .setTaskColumnIdx(1)
    .setStaffColumnIdx(5);

  var app = new Crawler(builder.setSheetName('管理_制作3')),
    app2 = new Crawler(builder.setSheetName('案件_企画4'));

  var result = app.getTaskDaily().concat(app2.getTaskDaily());
  var output = [];
  var dateList: string[] = result.map(values => values[0].date);
  result.forEach(values => {
    let sameDateExists = output.filter((val, index) => {
      return val[0].date === values[0].date;
    });

    if (sameDateExists[0]) {
      let existing_index = dateList.indexOf(values[0].date);
      output[existing_index] = output[existing_index].concat(values);
    } else {
      output.push(values);
    }
  });
  output.sort((array_a, array_b) => {
    return array_a[0].date > array_b[0].date ? 1 : -1;
  });
  Logger.log(output);
};

global.testGetData10DevTeam = () => {
  var _date = '2018-10-04';
  var builder = new Supplier();
  builder
    .setSheetId(2)
    .setFamilyName('小嶋')
    .setBeginningPeriod(new Date(_date + 'T00:00:00'))
    .setEndPeriod(new Date('2018-10-11T00:00:00'));

  builder
    .setStartOfColumn(9)
    .setStartOfRow(11)
    .setCategoryColumnIdx(1)
    .setProjectColumnIdx(0)
    .setTaskColumnIdx(2)
    .setStaffColumnIdx(4);

  var result = new Crawler(builder.setSheetName('スケジュール表')).getTaskDaily();
  var output = [];
  var dateList: string[] = result.map(values => values[0].date);
  result.forEach(values => {
    let sameDateExists = output.filter((val, index) => {
      return val[0].date === values[0].date;
    });

    if (sameDateExists[0]) {
      let existing_index = dateList.indexOf(values[0].date);
      output[existing_index] = output[existing_index].concat(values);
    } else {
      output.push(values);
    }
  });
  output.sort((array_a, array_b) => {
    return array_a[0].date > array_b[0].date ? 1 : -1;
  });
  Logger.log(output);
};
