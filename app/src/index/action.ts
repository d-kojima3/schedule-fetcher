import HtmlService = GoogleAppsScript.HTML.HtmlService;
import './gas.debug';
import { SheetConfig, UnavailableRangeError, ResponseType } from '../sheet.config';
import { processFormAndGetTask } from './app';

declare var global: any;

global.doGet = (request: object) => {
  return HtmlService.createTemplateFromFile('index')
    .evaluate()
    .setTitle('Sinnvoll - スケジュール取得ツール')
    .addMetaTag('viewport', 'width=device-width, initial-scale=1');
};

global.fetchResults = (request: {
  sheet_id;
  family_name;
  period_beginning;
  period_end;
}): ResponseType[][] | string => {
  var task: ResponseType[][] = [],
    output: ResponseType[][] = [];

  task = processFormAndGetTask(request);

  var dateList: string[] = task.map(values => values[0].date);
  task.forEach(values => {
    let sameDateExists = output.filter((val, index) => {
      return val[0].date === values[0].date;
    });

    if (sameDateExists[0]) {
      let existing_index = dateList.indexOf(values[0].date);
      output[existing_index] = output[existing_index].concat(values);
    } else {
      output.push(values);
    }
  });

  output.sort((array_a, array_b) => {
    return array_a[0].date > array_b[0].date ? 1 : -1;
  });
  return output;
};

global.getSheets = () => {
  return SheetConfig.sheets;
};

global.include = (filename: string) => {
  return HtmlService.createHtmlOutputFromFile(filename).getContent();
};
