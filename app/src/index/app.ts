import { Supplier } from '../supplier';
import { Crawler } from '../crawler';
import { ResponseType } from '../sheet.config';
import Spreadsheet = GoogleAppsScript.Spreadsheet.Spreadsheet;

export function processFormAndGetTask(form_data: {
  sheet_id;
  family_name;
  period_beginning;
  period_end;
}): ResponseType[][] {
  var builder = new Supplier();
  builder
    .setSheetId(form_data.sheet_id)
    .setFamilyName(form_data.family_name)
    .setBeginningPeriod(new Date(form_data.period_beginning + 'T00:00:00'))
    .setEndPeriod(new Date(form_data.period_end + 'T00:00:00'));

  switch (form_data.sheet_id) {
    case '1':
      builder
        .setStartOfColumn(9)
        .setStartOfRow(3)
        .setCategoryColumnIdx(0)
        .setProjectColumnIdx(0)
        .setTaskColumnIdx(1)
        .setStaffColumnIdx(5);

      var app = new Crawler(builder.setSheetName('管理_制作3')),
        app2 = new Crawler(builder.setSheetName('案件_企画4'));

      return app.getTaskDaily().concat(app2.getTaskDaily());

    case '2':
      builder
        .setStartOfColumn(9)
        .setStartOfRow(11)
        .setCategoryColumnIdx(1)
        .setProjectColumnIdx(0)
        .setTaskColumnIdx(2)
        .setStaffColumnIdx(4);

      return new Crawler(builder.setSheetName('スケジュール表')).getTaskDaily();
  }
}
